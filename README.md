# gulp_xlsx2sql

## 字段映射设置及配置参考 gulpfile.js
``` js
const gulp = require('gulp');
const xlsx2sql = require('gulp_xlsx2sql');
const concat = require('gulp-concat');
const fields = new Map();
// 自定义字段映射
fields.set('性别', 'sex');

gulp.task('default', function (call) {
    gulp.src('xlsx/**/*.xlsx')
        //表格转json
        .pipe(xlsx2sql({
            fieldRow: 1,                    // 数据表字段对应行
            dataRow: 2,                     // 数据表数据开始行
            mainTableName: 'mainweixin',    // 选择目录下（不包含子目录）xlsx文件对应数据表名
            dirToTableName: true,           // 按选择目录的子目录生成表名 
            xlsx_file: true,                // 是否创建数据来源文件名字段
            xlsx_file_as: 'aaaaa',          // 文件名字段设置别名
            trace: true,
        }, fields))
        .pipe(concat('all.sql'))
        .pipe(gulp.dest('msql'));
    call();
});
```
> * 同级目录下多个xlsx文件的数据会写入到同一个数据表中，主目录以mainTableName指定表名，子目录按目录名生成表名。数据表字段不存在按fieldRow指定行自动创建，中文字段名将自动转为拼音。

## 环境配置

### 下载安装Python[https://www.python.org/downloads/](https://www.python.org/downloads/)

### 下载安装C++桌面运行环境  参考[https://blog.csdn.net/weixin_42066950/article/details/116095391](https://blog.csdn.net/weixin_42066950/article/details/116095391)

### 下载安装Node[https://nodejs.org/en/download/](https://nodejs.org/en/download/)

### 修改npm源地址
``` shell
npm config set registry https://registry.npm.taobao.org
```
### 安装node-gyp
``` shell
npm install -g node-gyp
```
### 安装yarn
``` shell
npm install -g yarn
```
>设置国内源地址
``` shell
yarn config set registry https://registry.npm.taobao.org -g 
yarn config set sass_binary_site http://cdn.npm.taobao.org/dist/node-sass -g
```

### 下载项目
``` shell
 git clone https://gitee.com/i-o-i/gulp_xlsx2sql.git
```

### 项目目录打开命令行执行项目初始化命令
``` shell
yarn
```
### 项目目录打开命令行执行 xlsx 转 sql 命令
``` shell
yarn run dev
```



