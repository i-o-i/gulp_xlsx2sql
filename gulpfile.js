const gulp = require('gulp');
const xlsx2sql = require('./index.js');
const concat = require('gulp-concat');
const fields = new Map();
// 字段映射设置
fields.set('WXID', 'wxid');
fields.set('微信号', 'wxh');
fields.set('头像', 'wxtx');
fields.set('性别', 'wxxb');
fields.set('地区', 'wxdq');
fields.set('手机号', 'wxsj');
fields.set('个性签名', 'wxqm');
fields.set('头像原图链接', 'wxtxyt');
fields.set('朋友圈背景链接', 'wxbj');

gulp.task('default', function (call) {
    gulp.src('xlsx/**/*.xlsx')
        //表格转json
        .pipe(xlsx2sql({
            fieldRow: 1,
            dataRow: 2,
            dirToTableName: true,
            xlsx_file: true,
            xlsx_file_as: 'aaaaa',
            trace: true,
            mainTableName: 'mainweixin',
        }, fields))
        .pipe(concat('all.sql'))
        .pipe(gulp.dest('msql'));
    call();
});